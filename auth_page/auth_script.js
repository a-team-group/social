const authBtnNode = document.getElementById('auth-form-card-auth-btn');
const inputLoginNode = document.getElementById('auth-form-card-input-login');
const inputPassNode = document.getElementById('auth-form-card-input-password');
const errMsg = document.getElementById('invalid-data');
const regBtnNode = document.getElementById('auth-form-card-any-actions-reg');
const fgtPassBtnNode = document.getElementById('auth-form-card-any-actions-fgt-pass');
function showErrorMsg(message) {
    errMsg.hidden = false;
    errMsg.innerHTML = message;
    console.log(errMsg)
};
authBtnNode.addEventListener('click', function () {
    if ((inputLoginNode.value.length === 0) && (inputPassNode.value.length === 0)) {
        authBtnNode.style = 'margin-top: 7px;';
        showErrorMsg('Введите данные для авторизации!');
        return;
    }else if (inputLoginNode.value.length === 0){
        authBtnNode.style = 'margin-top: 7px;';
        showErrorMsg('Введите телефон или почту!');
    } else if ((inputPassNode.value.length === 0) || (inputPassNode.value.length < 6)) {
        authBtnNode.style = 'margin-top: 7px;';
        showErrorMsg('Пароль не меньше 6 символов!');
    } else {
        authBtnNode.style = 'margin-top: 35px;';
        errMsg.hidden = true;
    }
});
regBtnNode.addEventListener('click', function (){
   alert('ТУТ НИКОГО НЕТ!');
});
fgtPassBtnNode.addEventListener('click', function (){
   alert('ТУТ НИКОГО НЕТ!');
});
